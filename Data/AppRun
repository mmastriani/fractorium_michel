#!/bin/bash

# The purpose of this custom AppRun script is
# to allow symlinking the AppImage and invoking
# the corresponding binary depending on which
# symlink was used to invoke the AppImage

set -e

if [ ! -z "$DEBUG" ] ; then
  env
  set -x
fi

THIS="$0"
# http://stackoverflow.com/questions/3190818/
args=("$@")
NUMBER_OF_ARGS="$#"

CONFIG_PATH=~/.config/fractorium
DARK_LINUX=~/.config/fractorium/dark_linux.qss
LIGHT_DARK=~/.config/fractorium/lightdark.qss
URANIUM=~/.config/fractorium/uranium.qss

# please do not change $VENDORPREFIX as it will allow for desktop files
# belonging to AppImages to be recognized by future AppImageKit components
# such as desktop integration daemons
VENDORPREFIX=appimagekit

if [ -z $APPDIR ] ; then
  # Find the AppDir. It is the directory that contains AppRun.
  # This assumes that this script resides inside the AppDir or a subdirectory.
  # If this script is run inside an AppImage, then the AppImage runtime likely has already set $APPDIR
  path="$(dirname "$(readlink -f "${THIS}")")"
  while [[ "$path" != "" && ! -e "$path/$1" ]]; do
    path=${path%/*}
  done
  APPDIR="$path"
fi

#export PATH="${APPDIR}:${APPDIR}/usr/sbin:${PATH}"
#export XDG_DATA_DIRS="./share/:/usr/share/gnome:/usr/local/share/:/usr/share/:${XDG_DATA_DIRS}"
#export XDG_DATA_DIRS="${APPDIR}"/usr/share/:"${XDG_DATA_DIRS}":/usr/share/gnome/:/usr/local/share/:/usr/share/
#export GSETTINGS_SCHEMA_DIR="${APPDIR}/usr/share/glib-2.0/schemas:${GSETTINGS_SCHEMA_DIR}"

export LD_LIBRARY_PATH="${APPDIR}/usr/lib:${LD_LIBRARY_PATH}"

DESKTOP_FILE="$APPDIR/fractorium.desktop"

if [ -z "$APPIMAGE_EXIT_AFTER_INSTALL" ] ; then
  trap atexit EXIT
fi

atexit()
{
  if [ ! -z $APPIMAGE ] ; then

    BINARY_NAME=$(basename "$ARGV0")

    if [ $NUMBER_OF_ARGS -eq 0 ] ; then
       FINAL_ARGS=""
    else
       FINAL_ARGS="${args[@]}"
    fi

    #echo "args: " $FINAL_ARGS

    if [ -e "$APPDIR/usr/bin/$BINARY_NAME" ] ; then
      exec "$APPDIR/usr/bin/$BINARY_NAME" $FINAL_ARGS
    else
      exec "$APPDIR/usr/bin/fractorium" $FINAL_ARGS
    fi
  else  
    exec "$APPDIR/usr/bin/fractorium"
  fi
}

if [ ! -z $APPIMAGE ] ; then
  BINARY_NAME=$(basename "$ARGV0")
  if [[ ("$BINARY_NAME" == "emberrender") || ("$BINARY_NAME" == "embergenome") || ("$BINARY_NAME" == "emberanimate") ]] ; then      
      exit 0
  fi
fi

if [ ! -e "$DARK_LINUX" ] ; then
  mkdir -p $CONFIG_PATH
  cp $APPDIR/usr/bin/dark_linux.qss $CONFIG_PATH
fi

if [ ! -e "$LIGHT_DARK" ] ; then
  mkdir -p $CONFIG_PATH
  cp $APPDIR/usr/bin/lightdark.qss $CONFIG_PATH
fi

if [ ! -e "$URANIUM" ] ; then
  mkdir -p $CONFIG_PATH
  cp $APPDIR/usr/bin/uranium.qss $CONFIG_PATH
fi

check_prevent()
{
  FILE=$1
  if [ -e "$FILE" ] ; then
    exit 0
  fi
}

# exit immediately of one of these files is present
# (e.g., because the desktop environment wants to handle desktop integration itself)
check_prevent "$HOME/.local/share/$VENDORPREFIX/no_desktopintegration"
check_prevent "/usr/share/$VENDORPREFIX/no_desktopintegration"
check_prevent "/etc/$VENDORPREFIX/no_desktopintegration"
check_prevent "/usr/share/applications/Fractorium.desktop"
check_prevent "/usr/share/applications/fractorium.desktop"

# exit immediately if appimaged is running
pidof appimaged 2>/dev/null && exit 0

# exit immediately if $DESKTOPINTEGRATION is not empty
if [ ! -z "$DESKTOPINTEGRATION" ] ; then
  echo "no desktop integration."
  exit 0
fi

check_dep()
{
  DEP=$1
  if [ -z $(which $DEP) ] ; then
    echo "$DEP is missing. Skipping ${THIS}."
    exit 0
  fi
}

if [ ! -f "$DESKTOP_FILE" ] ; then
  echo "Desktop file is missing. Please run ${THIS} from within an AppImage."
  exit 0
fi

if [ -z "$APPIMAGE" ] ; then
  APPIMAGE="$APPDIR/AppRun"
  # not running from within an AppImage; hence using the AppRun for Exec=
fi

# determine where the desktop file should be installed
if [[ $EUID -ne 0 ]]; then
   DESTINATION_DIR_DESKTOP="$HOME/.local/share/applications"
   SYSTEM_WIDE=""
else
   DESTINATION_DIR_DESKTOP="/usr/local/share/applications"
   # for xdg-mime and xdg-icon-resource
   SYSTEM_WIDE="--mode system"
fi

desktopFilePath="$DESTINATION_DIR_DESKTOP/$VENDORPREFIX-fractorium.desktop"

# check if the desktop file is already there and if so, whether it points to the same AppImage
if [ -e "$desktopFilePath" ] ; then
  INSTALLED_PATH=$(grep "^Exec=" "$DESTINATION_DIR_DESKTOP/$VENDORPREFIX-fractorium.desktop" | head -n 1 | cut -d= -f2)
  RUNING_PATH=${APPIMAGE}

  #echo "installed: $INSTALLED_APP_VERSION image: $APP_VERSION"
  if [ "$INSTALLED_PATH" == "$RUNING_PATH" ] ; then
    #echo "INSTALLED_PATH nothing to do"
    exit 0
  fi
fi


# check whether dependencies are present in base system (we do not bundle these)
# http://cgit.freedesktop.org/xdg/desktop-file-utils/
check_dep desktop-file-install
check_dep xdg-icon-resource
check_dep xdg-mime
check_dep xdg-desktop-menu

desktop-file-install --rebuild-mime-info-cache \
  --vendor=$VENDORPREFIX --set-key=Exec --set-value=${APPIMAGE} \
  --set-key=X-AppImage-Comment --set-value="Generated by ${THIS}" \
  --set-icon="appimagekit-fractorium" --set-key=TryExec --set-value=${APPIMAGE// /\\s} "$DESKTOP_FILE" \
  --dir "$DESTINATION_DIR_DESKTOP" \
  --mode=755

# uninstall previous icons
xdg-icon-resource uninstall --noupdate --size 256 "appimagekit-fractorium"

# install the icon files

xdg-icon-resource install --noupdate --context apps --size 256 "$APPDIR/usr/share/icons/hicolor/256x256/apps/fractorium.png" "appimagekit-fractorium"

xdg-icon-resource forceupdate

# Install the icon files for the mime type
ICONS=$(find "${APPDIR}/usr/share/icons/" -wholename "*/mimetypes/*.png" 2>/dev/null || true)
for ICON in $ICONS ; do
  ICON_SIZE=$(echo "${ICON}" | rev | cut -d "/" -f 3 | rev | cut -d "x" -f 1)
  xdg-icon-resource install --context mimetypes --size ${ICON_SIZE} "${ICON}" $(basename $ICON | sed -e 's/.png//g')
done

xdg-desktop-menu forceupdate
# for MIME
gtk-update-icon-cache


